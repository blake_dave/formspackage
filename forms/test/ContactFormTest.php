<?php

namespace Tests\Unit;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;


class ContactFormTest extends TestCase
{
    /**
     Simple test for the Contact form
     */
    public function testFormSubmit()
    {

	$this->visit('/contact')
		->type('Name_test', 'name')
		->type('test@example.com', 'email')
		->type('Message_test', 'message')
		->press('Submit')
		->see('Thank you')
		->seeInDatabase('contact', ['email' => 'test@example.com']);
	
	

    }
}
