<?php

namespace DTB\Forms;

use Illuminate\Http\Request;
use Illuminate\Html;
use App\Http\Controllers\Controller;
use Mail;


class ContactFormController extends Controller
{
    	public function index()
	{	
		echo("Forms Controller...");
	}

	private $mail_from = 'blake_dave@hotmail.com';

    	public function create()
    	{
        	return view('Forms::ContactForm');
    	}

   	public function store(Request $request)
    	{

		$this->validate($request, ['name' => 'required', 'email' => 'required|email', 'message' => 'required']);

		//  Create a new Contact and save to database
		$contact = new Contact;
		$contact->name = $request->input('name');
		$contact->email = $request->input('email');
		$contact->user_message = $request->input('message');
		$contact->save();

		//  Email details to default address or the one set above
		Mail::to(config('formsConfig.forms_to_email_address'))->send(new FormEmail($contact));

		//  Confirmation message for the user
		return \Redirect::route('contact')
	      	->with('message', 'Thank you for contacting us!');

    	}
}
