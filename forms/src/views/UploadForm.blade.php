<h1>Upload a file</h1>

@if(Session::has('message'))
    <div>
      {{Session::get('message')}}
    </div>
@else

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'upload_store', 'class' => 'UploadForm', 'enctype' => 'multipart/form-data')) !!}

<div>
    {!! Form::label('Name') !!}
    {!! Form::text('name', null, 
        array('required',  
              'placeholder'=>'Your name')) !!}
</div>

<div>
    {!! Form::label('Email address') !!}
    {!! Form::text('email', null, 
        array('required',  
              'placeholder'=>'Your email address')) !!}
</div>

<div>
    {!! Form::label('Message') !!}
    {!! Form::textarea('message', null, 
        array('placeholder'=>'Your message')) !!}
</div>

<div>
    {!! Form::label('report file') !!}
    {!! Form::file('report', null) !!}
</div>

<div>
    {!! Form::submit('Submit') !!}
</div>
{!! Form::close() !!}

@endif
