<h1>Contact</h1>

@if(Session::has('message'))
    <div>
      {{Session::get('message')}}
    </div>
@else

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'contact_store', 'class' => 'ContactForm')) !!}

<div>
    {!! Form::label('Name') !!}
    {!! Form::text('name', null, 
        array('required',  
              'placeholder'=>'Your name')) !!}
</div>

<div>
    {!! Form::label('Email address') !!}
    {!! Form::text('email', null, 
        array('required',  
              'placeholder'=>'Your email address')) !!}
</div>

<div>
    {!! Form::label('Message') !!}
    {!! Form::textarea('message', null, 
        array('required',  
              'placeholder'=>'Your message')) !!}
</div>

<div>
    {!! Form::submit('Submit') !!}
</div>
{!! Form::close() !!}

@endif
