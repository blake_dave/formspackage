<?php

namespace DTB\Forms;

use Illuminate\Support\ServiceProvider;

class FormsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
	$this->publishes([__DIR__.'/formsConfig.php' => config_path('formsConfig.php')]);
        $this->loadViewsFrom(__DIR__.'/views', 'Forms');
	$this->loadMigrationsFrom(__DIR__.'/');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
	$this->app->make('DTB\Forms\ContactFormController');
	$this->app->make('DTB\Forms\UploadFormController');
    }
}
