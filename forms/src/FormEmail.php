<?php

namespace DTB\Forms;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FormEmail extends Mailable
{
	use Queueable, SerializesModels;

	public $form;
	/**
	* Create a new message instance.
	*
	* @return void
	*/
	 public function __construct($form)
	{
		$this->form = $form;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build()
	{
		if (get_class($this->form) == 'DTB\Forms\Upload')
	{
		return $this->subject('File upload from website')->view('Forms::UploadEmail');
	}
	
	return $this->subject('Contact message from website')->view('Forms::ContactEmail');


	//return $this->subject('File upload from website')->view('Forms::UploadEmail');
	//return $this->subject('File upload from website')->view('Forms::ContactEmail');
	}
	
}
