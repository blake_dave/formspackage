<?php

namespace DTB\Forms;

use Illuminate\Http\Request;
use Illuminate\Html;
use App\Http\Controllers\Controller;
use Mail;


class UploadFormController extends Controller
{
    	public function index()
	{	
		echo("Forms Controller - Upload");
	}

	private $mail_from = 'blake_dave@hotmail.com';

    	public function create()
    	{
		
        	return view('Forms::UploadForm');
	
    	}

   	public function store(Request $request)
    	{

		$this->validate($request, ['name' => 'required', 'email' => 'required|email', 'report'=>'required|file']);

		request()->file('report')->store('reports');

		//  Create a new Contact and save to database
		$upload = new Upload;
		$upload->name = $request->input('name');
		$upload->email = $request->input('email');
		$upload->file_name = $request->file('report')->getClientOriginalName();
		$upload->user_message = $request->input('message');
		$upload->save();

		//  Email details to default address or the one set above
		Mail::to(config('formsConfig.forms_to_email_address'))->send(new FormEmail($upload));

		//  Confirmation message for the user
		return \Redirect::route('upload')
	      	->with('message', 'Thank you for uploading your file!');

    	}
}
