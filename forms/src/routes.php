<?php


//Route::get('/upload', 'DTB\Forms\UploadFormController@index');

Route::group(['middleware' => ['web']], function () {
    	Route::get('/contact', ['as' => 'contact', 'uses' => 'DTB\Forms\ContactFormController@create']);
	Route::post('/contact', ['as' => 'contact_store', 'uses' => 'DTB\Forms\ContactFormController@store']);
	
	Route::get('/upload', ['as' => 'upload', 'uses' => 'DTB\Forms\UploadFormController@create']);
	Route::post('/upload', ['as' => 'upload_store', 'uses' => 'DTB\Forms\UploadFormController@store']);
});


