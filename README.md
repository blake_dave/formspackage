# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*  TDI Tuning test
*  Simple package that provides 2 forms at /contact and /upload
*  Forms email user input and also persist to database

### How do I get set up? ###

* Not sure how to get composer to install this..
* Create a folder dtb in vendor and put my forms folder in it
* Add line to composer.json  "DTB\\Forms\\":"vendor/dtb/forms/src"
* Add my Service Provider to app.php  DTB\Forms\FormsServiceProvider::class,
* Run composer require "laravelcollective/html":"^5.4.0"
* Run composer update
* If database tables not create the run migrate and if formConfig.php is not moved to config folder, move it manually
* Forms send email to address specified in .env or you can change it in the above config
* Email and database have to be set in .env file

